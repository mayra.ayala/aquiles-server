//const { Console } = require("console");
let np,points=1,status;
let pointcounter = points;
let model_id=0;
let actualpoint=1;
let net_path;
let samplenumbers;
let resultadodemuestras=true;
let webcamid;
let flag42q=false //Bandera para modulo de 42Q

//********************************************************************** Web animation functions

//-----* Popups T1 (Escaneo)
var btnTeachMode = document.getElementById ('tmode'),
	inspectionMode = document.getElementById ('imode'),
	inspectionModeh = document.getElementById ('inspeccion'),
    overlay1 = document.getElementById ('overlay1'),
    popupscan = document.getElementById ('popupscan'), 
	btn_cerrar_popupscan = document.getElementById ('btn-cerrar-popupscan');
	h4T1=document.getElementById ('value_captured'),
    h4T1e=document.getElementById ('value_captured_error'),	
	btnokT=document.getElementById('btnOkT');
	btnokI=document.getElementById('btnOkI');
	selectedbutton=0;

//Teach mode function button	
btnTeachMode.addEventListener('click', function(){
       overlay1.classList.add('active');
       popupscan.classList.add('active');
	   inspectionModeh.innerHTML="TEACH MODE";
	   btnokI.style.visibility="hidden";
	   selectedbutton=true;
	   console.log("TEACH"+" "+ selectedbutton);
});

//Inspection mode function button	
inspectionMode.addEventListener('click', function(){		
       overlay1.classList.add('active');
       popupscan.classList.add('active');
	   inspectionModeh.innerHTML="INSPECTION MODE";
	   btnokT.style.visibility="hidden"
	   selectedbutton=false;//bandera de evaluacion 
	   console.log("INSPECCION"+" "+ selectedbutton);
});

document.getElementById('serial').addEventListener('keypress', function(event) {
      
	   if ((selectedbutton==true)&&(event.keyCode == 13)){		                    
		
		event.preventDefault();			
		valuescanT();           
		getpoints();
		
		}
	  
	   if ((selectedbutton==false)&&(event.keyCode == 13)){ 		          
		
		event.preventDefault();	
		valuescanI();
		getpoints();
		
		}
	    
		 });
						
//OkT function button	
btnokT.addEventListener('click', function(e){
    e.preventDefault();
	valuescanT(); 
    getpoints();
    
});

//OkI function button	
btnokI.addEventListener('click', function(e){
	setTimeout(socket.on,3000);
	e.preventDefault();
	valuescanI();
});

//X function button	
btn_cerrar_popupscan.addEventListener('click', function(e){
    
	e.preventDefault();
	location.reload();
});

//-----* Popups T2 ( Captura puntos)
var btnAbrirPopup = document.getElementById('btn-abrir-popup'),
	h3T2_np = document.getElementById('h3_model'),
	h3T2_status = document.getElementById('h3_status'),
	overlay2 = document.getElementById('overlay2'),
	popup2 = document.getElementById('popup2'),
	btnCerrarPopup2 = document.getElementById('btn-cerrar-popup2'),
	numero = document.getElementById('numero'),	
	atras = document.getElementById('atras'),	
	btnContinuar2 = document.getElementById('continuar2'),
	capturar = document.getElementById('capturar');
	
//Muestra valores de BD
function T2_window_open(){
	overlay2.classList.add('active');
	popup2.classList.add('active');
	h3T2_np.innerHTML ="Modelo: "+np+"";
	h3T2_status.innerHTML ="Estatus: "+status+"";
	numero.value=points;
	
//Habilita o no boton de captura.
	if (points == "")
	{ capturar.style.visibility="visible";}
	else
	{ capturar.style.visibility="hidden";}
}

//Enter event function
document.getElementById('numero').addEventListener('keypress', function(event) {
        if (event.keyCode == 13) {        
			event.preventDefault();
			pointscapture();
	setpoints();
	capturar.style.visibility="hidden";
			numero.value="";
        }
    });

//X function button
function T2_window_close(){

	numero.value="";
	location.reload();
}

//Captura (Toma valores ingresados)
capturar.addEventListener('click', function(e){
    e.preventDefault();
	pointscapture();
	setpoints();
	capturar.style.visibility="hidden";
	numero.value="";
});

//Atras 
function T2_atras(){
//Cierra T2
	overlay2.classList.remove('active');
	popup2.classList.remove('active');
//Abre T1
	overlay1.classList.add('active');
    popupscan.classList.add('active');
}

//Siguiente (Involucra el inicio de T4) 
function next(){
	overlay2.classList.remove('active');
	popup2.classList.remove('active');
	numero.value="";
	open_4T();
//Manda llamar siguiente ventana T3
}

//-----* Popups T4 (Samples trainer)
const webcam_4T = document.getElementById('webcam');
var btnAbrirPopup4T = document.getElementById('btn-abrir-popup4T'),
	overlay4T = document.getElementById('overlay4T'),
	popup4T = document.getElementById('popup4T'),
	btnCerrarPopup4T = document.getElementById('btn-cerrar-popup4T');
	h4T4_np=document.getElementById('T4model');
	h4T4_status=document.getElementById('T4point');
	
//Pantalla inicio T4
function open_4T(){
	
	document.images["onoffT4"].src = "img/off.jpg";
	document.getElementById('T4samples').style.visibility="hidden";
	document.getElementById('T4learning').style.visibility="hidden";
	document.getElementById('minusT4').style.visibility="hidden";
	document.getElementById('plusT4').style.visibility="hidden";
	document.getElementById('refreshnnT4').style.visibility="hidden";
	document.getElementById('Mala4T').style.visibility="hidden";
	document.getElementById('Buena4T').style.visibility="hidden";
	document.getElementById('Guardar4T').style.visibility="hidden";
	document.getElementById('hamburguerT4').style.visibility="hidden";
	document.getElementById('hamburguer-submenuT4').style.visibility="hidden";
	document.getElementById('nextT4').style.visibility="hidden";
	movehw(0);//Regresa a home el robot
//const webcam4T = tf.data.webcam(webcam_4T);
	getmid();
	overlay4T.classList.add('active');
	popup4T.classList.add('active');
	h4T4_np.innerHTML ="Modelo: "+np+"";
	h4T4_status.innerHTML ="Punto(s): "+pointcounter+" de "+points+"";
	ml();
	//defaultcam();
}

//X
btnCerrarPopup4T.addEventListener('click', function(e){
	e.preventDefault();
	location.reload();
});

function close5T(){
	overlay5T.classList.remove('active');
	popup5T.classList.remove('active');
}

function closeT4(){
	
	overlay4T.classList.remove('active');
	popup4T.classList.remove('active');
}

//-----* Popups T5 (Net prediction evaluation)
const webcam_5T = document.getElementById('webcam5T');
var btnAbrirPopup5T = document.getElementById('btn-abrir-popup5T'),
	overlay5T = document.getElementById('overlay5T'),
	popup5T = document.getElementById('popup5T'),
	btnCerrarPopup5T = document.getElementById('btn-cerrar-popup5T');
	h4T5_np=document.getElementById('T5model');
	h4T5_status=document.getElementById('T5point');

function open5T(){
	
	if(points!= ""){
		
		document.getElementById('minusT5').style.visibility="hidden";
		document.getElementById('plusT5').style.visibility="hidden";
		document.getElementById('T5learning').style.visibility="hidden";
		document.getElementById('refreshnn').style.visibility="hidden";
		document.getElementById('Mala5T').style.visibility="hidden";
		document.getElementById('Buena5T').style.visibility="hidden";
		document.getElementById('Terminar5T').style.visibility="hidden";
		document.getElementById('hamburguerT5').style.visibility="hidden";
		document.getElementById('hamburguer-submenuT5').style.visibility="hidden";
		document.getElementById('icon42Q').style.visibility="hidden";
		
		overlay5T.classList.add('active');
		popup5T.classList.add('active');
		h4T5_np.innerHTML ="Modelo: "+np+"";
		setTimeout(function(){h4T5_status.innerHTML ="Punto(s): "+pointcounter+" de "+ points +"";},300);
		//const webcam5Ton = tf.data.webcam(webcam_5T,xid);
		defaultcamID();
		getsampleqty();
		netvarpathset();
		tensorcamactivate();		
		movehw(0);//Pone a home el robot
		//ml();
	}
	
}

//X
btnCerrarPopup5T.addEventListener('click', function(){
	location.reload();
});

//on/off button
let img="on.jpg";
function onoff(){			
				
  if ( img == "on.jpg" ) {
	        //ON
			document.images["onoff"].src = "img/on.jpg";
			img  = "off.jpg";	
			samplesT5();//obtiene numero de muestras del modelo en cuestion			
			call();	//Manda llamar red neuronal del punto a revisar	
					
			movehw(pointcounter);	//Mueve el robot al primer punto 	
			document.getElementById('T5samples').style="unhidden";
			document.getElementById('T5learning').style="unhidden";
			document.getElementById('minusT5').style="unhidden";
			document.getElementById('plusT5').style="unhidden";
			document.getElementById('refreshnn').style="unhidden";
			document.getElementById('Mala5T').style="unhidden";
			document.getElementById('Buena5T').style="unhidden";'hamburguerT5'
			document.getElementById('hamburguerT5').style="unhidden";
			document.getElementById('hamburguer-submenuT5').style="unhidden";
			}
  		else {
			//OFF
    		document.images["onoff"].src = "img/off.jpg";
    		img = "on.jpg";
			movehw(0);//Regresa a home el robot
			document.getElementById('T5samples').style.visibility="hidden";
			document.getElementById('T5learning').style.visibility="hidden";
			document.getElementById('minusT5').style.visibility="hidden";
			document.getElementById('plusT5').style.visibility="hidden";
			document.getElementById('refreshnn').style.visibility="hidden";
			document.getElementById('Mala5T').style.visibility="hidden";
			document.getElementById('Buena5T').style.visibility="hidden";
			document.getElementById('Terminar5T').style.visibility="hidden";
			document.getElementById('hamburguerT5').style.visibility="hidden";
			document.getElementById('hamburguer-submenuT5').style.visibility="hidden";
    		
  			}
		}
function onoffT4(){			
				
  if ( img == "on.jpg" ) {
	        //ON
			document.images["onoffT4"].src = "img/on.jpg";
			img  = "off.jpg";	
			//samplesT5();//obtiene numero de muestras del modelo en cuestion			
			//call();	//Manda llamar red neuronal del punto a revisar			
			movehw(pointcounter);	//Mueve el robot al primer punto 	
			document.getElementById('T4samples').style="unhidden";
			document.getElementById('T4learning').style="unhidden";
			document.getElementById('minusT4').style="unhidden";
			document.getElementById('plusT4').style="unhidden";
			document.getElementById('refreshnnT4').style="unhidden";
			document.getElementById('Mala4T').style="unhidden";
			document.getElementById('Buena4T').style="unhidden";'hamburguerT4'
			document.getElementById('hamburguerT4').style="unhidden";
			document.getElementById('hamburguer-submenuT4').style="unhidden";
			document.getElementById('Guardar4T').style="unhidden";
			document.getElementById('nextT4').style="unhidden";
			}
  		else {
			//OFF
    		
    		img = "on.jpg";
			document.images["onoffT4"].src = "img/off.jpg";
			movehw(0);//Regresa a home el robot
			document.getElementById('T4samples').style.visibility="hidden";
			document.getElementById('T4learning').style.visibility="hidden";
			document.getElementById('minusT4').style.visibility="hidden";
			document.getElementById('plusT4').style.visibility="hidden";
			document.getElementById('refreshnnT4').style.visibility="hidden";
			document.getElementById('Mala4T').style.visibility="hidden";
			document.getElementById('Buena4T').style.visibility="hidden";
			document.getElementById('Guardar4T').style.visibility="hidden";
			document.getElementById('hamburguerT4').style.visibility="hidden";
			document.getElementById('hamburguer-submenuT4').style.visibility="hidden";
			document.getElementById('nextT4').style.visibility="hidden";
    		
  			}
		}

//********************************************************************** Test sequence functions
function varvals(){
	console.log("NP: "+np);
	console.log("Activepoint: "+ actualpoint);
	console.log("Inspointsqty: "+ points);
	console.log("Model ID: "+ model_id);
	console.log("Net path: "+net_path);
	console.log("Samples: "+ sampleqty);
}

function valuescanT(){
	 np = document.getElementById('serial').value;
	 console.log("np: "+np);
	 
 if(np==""){
	h4T1.innerHTML ="Por favor, escanea la unidad.";
		 }
	
 else {
	getpn();
	//funcion de query para obtener puntos si es que los hay
	overlay1.classList.remove('active');
	popupscan.classList.remove ('active');
	setTimeout(T2_window_open,200);
	h4T1.innerHTML ="";
	}

	document.getElementById('serial').value = null ; 
 }
 
// Ubica test location UUT
 
function valuescanI(){
	 np = document.getElementById('serial').value;
	 console.log("np: "+np);
	 
if(np==""){
	h4T1.innerHTML ="Ingresa un numero de parte";
	 }
	 
else {	 
	getpn();//funcion de query para obtener np si esta en la base
	getmid();
	setTimeout(getpoints,400);

	setTimeout(function evaluastatus(){//Inicia Settime out de evaluastatus
		if (status == "Encontrado"){
			
			open5T();
			ml();			
			h4T1.innerHTML ="";
			}
		else
			{
			h4T1.innerHTML ="Numero de parte no existe en BD";
			 }
		},700);//Cierra Settime out de evaluastatus
	document.getElementById('serial').value = null ; 	
 }// Ubica test location UUT
} 

function netvarpathset(){
	net_path="Aquiles/samples/"+np+"_net"+"_point_"+actualpoint+"_neural_network.json"
}

function pointscapture(){
	 points = document.getElementById('numero').value;
	 console.log("Points: "+points);
	 
	 if(points==""){
		h3T2_status.innerHTML = "Estatus: Ingresa los puntos a inspeccionar.";
			 }
	 else{
		h3T2_status.innerHTML ="Estatus: Capturado";
		numero.value="";		
		}
	document.getElementById('numero').value = null ; 
	
 }// Ubica test location UUT

function pointcounterincrementT4(){	
	if (pointcounter < points){
		pointcounter++;
		actualpoint=pointcounter;
		h4T4_status.innerHTML ="Punto(s): "+pointcounter+" de "+points+"";
		getsampleqty();
		movehw(pointcounter);
	}	
}

function pointcounterdecrementT4(){	
	if (pointcounter > 1){
		pointcounter--;
		actualpoint=pointcounter;
		h4T4_status.innerHTML ="Punto(s): "+pointcounter+" de "+points+"";
		getsampleqty();
		movehw(pointcounter);
	}	
}

function pointcounterincrementT5(){	
	if (pointcounter < points){
		pointcounter++;
		actualpoint=pointcounter;
		h4T5_status.innerHTML ="Punto(s): "+pointcounter+" de "+points+"";
		getsampleqty();
		movehw(pointcounter);
		//-- Switch net if exist
		if(samplenumbers!=0){
			setTimeout(call,700);
		}
		else{console.log("No puede cargar net sin samplesqty");}
		
	}	
}

function pointcounterdecrementT5(){	
	if (pointcounter > 1){
		pointcounter--;
		actualpoint=pointcounter;
		h4T5_status.innerHTML ="Punto(s): "+pointcounter+" de "+points+"";
		getsampleqty();
		movehw(pointcounter);
		if(samplenumbers!=0){
			setTimeout(call,700);
		}
		else{console.log("No puede cargar net sin samplesqty");}
		
	}	
}

function teachend(){
	eraseupdated();//borra el registro de la base de datos del punto y modelo actual
	deletenetfile();//borra el archivo JSON del entrenamiento de la carpeta samples
	console.log("Archivo borrado");
	setTimeout(nettofile(),1000);// Envia el JSON para guardalo en el servidor Samples
	console.log("Archivo actualizado");
	updatesampleqty();//
	console.log("New sampleqty sent");
	document.getElementById('Terminar5T').style.visibility="hidden";//Pokayoke para boton despues de presionar
}

function blockfunctionsminus(){
	
	pointcounterdecrementT5();
	samplesT5();
	netvarpathset();
	getsampleqty();

}

function blockfunctionsplus(){
	
	pointcounterincrementT5();
	samplesT5();
	netvarpathset();
	getsampleqty();

}

function validatesetupconfigurations(){
	//***************Variables locales de modbus */
	let modbus_ip = localStorage.getItem('Modbus_IP')
	let modbus_port = localStorage.getItem('Modbus_Port')
	let modbus_xpointreg = localStorage.getItem('Point_reg')
	let modbus_inpositionreg = localStorage.getItem('InPosition_reg')
	
	if(modbus_ip!=undefined&&modbus_port!=undefined&&modbus_xpointreg!=undefined&&modbus_inpositionreg!=undefined){
		
		modbus_on();
	}
	else
	{
		modbus_off();
	}		
	//***************Variables locales de 42Q*/

	let project_42Q = localStorage.getItem('project42Q');
	let unit_42Q = localStorage.getItem('unit42Q');

	if(project_42Q!=undefined&&unit_42Q!=undefined){
		on_42Q()
		flag42q=true
	}
	else
	{
		off_42Q()
		flag42q=false
	}



}

//********************************************************************** Funciones to Backbone 
const socket = io();

//-----* DB Querys

function postgon(){
	socket.emit('connectdbon');
	console.log("Inicia Conexion a DB");
}

function postgoff(){
	socket.emit('connectdboff');
	console.log("Termina Conexion a DB");
}

function getpn(){//pg migrated
    
    socket.emit('getpn',np);
	}

function setpn(){//pg migrated
	socket.emit('setpn',np);
	}

function setpoints(){//pg migrated
	socket.emit('setpoints',np,points);
	}

function getpoints(){//pg migrated	
	socket.emit('getpoints',np);
	}

function getmid(){//pg migrated
	socket.emit('getmid',np);
	}

function setmidpone(){//pg migrated	
	
	socket.emit('setmipone',model_id,actualpoint,sampleqty,net_path,webcamid);//net_path,
	console.log("mId: "+model_id+",Point: "+actualpoint+",Samples:"+sampleqty+",Netpath:"+net_path+",CamID:"+webcamid);
	
	}
	
function getsampleqty(){//pg migrated
	
	socket.emit('sampleqty',model_id,actualpoint);
	}

function eraseupdated(){//pg migrated

	socket.emit('eraseregister',model_id,actualpoint);
}

function updatesampleqty(){
	socket.emit('updatesampleqty',sampleqty,model_id,actualpoint);
}

function getcamid(){
	
	return new Promise(async resolve =>{
	socket.emit('getcamid',model_id,pointcounter);
	console.log("Esperando evento...");
	document.addEventListener('keyup', (e) => {resolve('resolved');console.log("Pressed");});
	});//Cierra Promise principal
}

//-----* Backbone Reply
socket.on('Qrypnresponse',function(targets){//pg migrated
	
	if (targets == 0) {
	status="Nuevo numero de parte.";	
	}
	else
	{	
	status="Encontrado";	
	}	
	});

socket.on('setpnresponse',function(targets){//pg migrated
	
	if (targets!= 0) {
	console.log("Numero de parte insertado.");
	}
	else{	
	console.log("Numero de parte duplicado.");}});

socket.on('Qrympointsresponse',function(targets){//pg migrated
	
	if (targets!= 0) {
	console.log("Puntos:"+targets);
	points=targets;
	}
	
	else{
	console.log("No hay");
	points="";
	}});

socket.on('Setpointsresponse',function(targets){//pg migrated
	
	if (targets!= 0) {
	console.log("Nuevo PN insertado");
	}
	else{	
	console.log("NP no insertado");
	
	}});
	
socket.on('Qrymidresponse',function(targets){//pg migrated
	
	if (targets!= 0) {
	model_id=targets;
	console.log("Qry Model_id: "+ model_id);
	}
	else{	
	console.log("NP sin id?"+targets);
	
	}});

socket.on('netpath',function(targets){
	
	if (targets!= 0) {
	net_path=targets;
	console.log("Valor de Netpath:"+net_path);
	
	}
	else{	
	console.log("Netpath bug?"+targets);
	
	}});

socket.on('sampleqtyresponse',function(targets){
	
	if (targets!= 0) {
	console.log("Sqty:"+targets);
	samplenumbers=targets;
	resultadodemuestras=true;
	}
	else{	
	console.log("Punto: "+actualpoint+" no tiene muestras");
	document.getElementById('T5samples').innerText = "Pass sample : 0\nFail sample : 0";
	resultadodemuestras=false;
	}});

socket.on('eraseregisterresponse',function(targets){
	
	if (targets == 1) {
	console.log("P erased: " + actualpoint);
	}
	else{	
	console.log("P not erased: " + actualpoint);
	}});

socket.on('getcamidresponse',function(camid){//
	
	if (camid!= 0) {
	cam_id=camid;
	webcamid=cam_id;
	console.log("DB camera id found-->: "+ webcamid);	
	setTimeout(remoteflag,300);		
	}
	else{	
	console.log("Camera id found-->"+webcamid);
	setTimeout(remoteflag,300);	
	}});
	
socket.on('Timsequence_start',function(flag){//pg migrated
	
	if (flag!= 0) {
	remoteflag();//Activa bandera para continuar
	//console.log("HW in position");
	}
	else{	
	console.log("HW flag not rised.");
	}});

socket.on('workstationresponse',function(data){
	console.log(data);
	});

//********************************************************************** Machine Learning
const webcamElement = document.getElementById('webcam');
const webcamElement5T = document.getElementById('webcam5T');
const classifier = knnClassifier.create();
let sampleqty=0;
let xid= 0;//variable usada para identificar camara utilizada

async function ml() {

  // Load the model.
  net = await mobilenet.load();  
  console.log('Neural network load success...');
  
  await defaultcamID();
  
  // Create an object from Tensorflow.js data API which could capture image 1
  // from the web camera as Tensor.
   
  let webcam = await tf.data.webcam(webcamElement,xid);//defaultcam();
  
  // Reads an image from the webcam and associates it with a specific class
  // index.
  const addExample = async classId => {
  // Capture an image from the web camera.
     let img = await webcam.capture();
	 
  // Get the intermediate activation of MobileNet 'conv_preds' and pass that
  // to the KNN classifier.
    const activation = net.infer(img, 'conv_preds');

  // Pass the intermediate activation to the classifier.
    classifier.addExample(activation, classId);

  // Dispose the tensor to release the memory.
    img.dispose();
	
  };

  // When clicking a button, add an example for that class.
  document.getElementById('Buena4T').addEventListener('click', () => {addExample(0);});//imgcap();console.log(kgucount++);
  document.getElementById('Mala4T').addEventListener('click', () => {addExample(1);});//imgcap();console.log(defectcount++);
  document.getElementById('Buena5T').addEventListener('click', () => {addExample(0);});//imgcap();console.log(kgucount++);
  document.getElementById('Mala5T').addEventListener('click', () => {addExample(1);});//imgcap();console.log(defectcount++);
 /* 
  while (true) {
    if (classifier.getNumClasses() > 0) {
      const img = await webcam.capture();
	  
      // Get the activation from mobilenet from the webcam.
      const activation = net.infer(img, 'conv_preds');
      // Get the most likely class and confidences from the classifier module.
      const result = await classifier.predictClass(activation);

      const classes = ['Pass','Fail'];
      document.getElementById('T4learning').innerText = `Learned(%): ${classes[result.label]} -> ${result.confidences[result.label]*100}`;
	  document.getElementById('T5learning').innerText = `Learned(%): ${classes[result.label]} -> ${result.confidences[result.label]*100}`;
	   
	  // console.log("Clase:",result);
	  // console.log("Probabilidad:",result.confidences[result.label]);//result.label
	  // console.log("Probabilidad:",probability);  
      // Dispose the tensor to release the memory.
      img.dispose();
	 // classifier.dispose();
    }
    await tf.nextFrame();
  }*/

}

//------- Function to predict just once
async function predict(){
	
	
	if (resultadodemuestras==true){//if logic
	
	return new Promise(async resolve =>{	
	
	
	const webcam = await tf.data.webcam(webcamElement,xid);
	
	const img = await webcam.capture();
	
	// Get the activation from mobilenet from the webcam.
	const activation = net.infer(img, 'conv_preds');
	// Get the most likely class and confidences from the classifier module.
	const result = await classifier.predictClass(activation);
	const classes = ['Pass','Fail'];
	document.getElementById('T4learning').innerText = `Learned(%): ${classes[result.label]} -> ${result.confidences[result.label]*100}`;
	document.getElementById('T5learning').innerText = `Learned(%): ${classes[result.label]} -> ${result.confidences[result.label]*100}`;
	result_values.push(`Punto ${pointcounter} : ${classes[result.label]}\n`);
	img.dispose();	
	document.getElementById('T5learning').style="unhidden";
	document.getElementById('T4learning').style="unhidden";
	setTimeout(function vanishprediction(){document.getElementById('T5learning').style.visibility="hidden"
				document.getElementById('T4learning').style.visibility="hidden"
				},2000);
	
	setTimeout(function fire(){resolve('resolved')},500);
	//document.addEventListener('keyup', (e) => {if (e.code === "ArrowUp") resolve('resolved');});
	});//Cierra Promise						
	}//if logic end
	else {
		// No hace nada
		}
}

//funcion temporal para la demo cuando no hay muestras es un caso distinto para T4 y no debe omitir leyendas %
async function predictT4d(){
	
	
	
	document.getElementById('T4learning').style.visibility="visible";		
	let webcam = await tf.data.webcam(webcamElement,xid);
	const img = await webcam.capture();
	
	// Get the activation from mobilenet from the webcam.
	const activation = net.infer(img, 'conv_preds');
	
	// Get the most likely class and confidences from the classifier module.
	const result = await classifier.predictClass(activation);
	const classes = ['Pass','Fail'];
	document.getElementById('T4learning').innerText = `Learned(%): ${classes[result.label]} -> ${result.confidences[result.label]*100}`;
	document.getElementById('T5learning').innerText = `Learned(%): ${classes[result.label]} -> ${result.confidences[result.label]*100}`;
	img.dispose();	
	document.getElementById('T5learning').style="unhidden";
	document.getElementById('T4learning').style="unhidden";
	setTimeout(function vanishprediction(){document.getElementById('T5learning').style.visibility="hidden"
				document.getElementById('T4learning').style.visibility="hidden"
				},3000);								
	//webcam.stop();
}// fin de funcion temporal

//------- Function to reset clasifier
function resetclassifier(){
	
	classifier.clearClass(0);
	classifier.clearClass(1);
	document.getElementById('T4samples').style.visibility="hidden";
	document.getElementById('T4learning').style.visibility="hidden";
		
}

function resetclassifierT5(){
	
	classifier.clearClass(0);
	classifier.clearClass(1);
	document.getElementById('T4samples').style.visibility="hidden";
	document.getElementById('T4learning').style.visibility="hidden";
	h4T5_status.innerHTML ="Punto(s): "+pointcounter+" de "+points+"";
	document.getElementById('T5samples').innerText = "Pass sample : "+samplenumbers+"\nFail sample : "+samplenumbers;
	call();
		
}

// Function to show samples qty
function samples(){	
	setTimeout(muestras4T,100);
 }
 
function muestras4T(){
	

	
	document.getElementById('T4learning').style.visibility="visible";
	let samplesneural=classifier.getClassExampleCount();
	if((samplesneural[0] && samplesneural[1])!= undefined){
	document.getElementById('T4samples').innerText = "Pass sample : "+samplesneural[0]+"\nFail sample : "+samplesneural[1];
	sampleqty=samplesneural[1];
	document.getElementById('T4samples').style.visibility="visible";	
	}
	if (samplesneural[0] != samplesneural[1]){
	document.getElementById('Guardar4T').style.visibility="hidden";	 
	document.getElementById('T4padlock').style.visibility="visible"; 
	document.getElementById('T4padlock').innerText = "¡ El numero de muestras buenas debe ser igual al numero de muestras malas ! ";
    }
    else if (samplesneural[0] == samplesneural[1]){
	document.getElementById('Guardar4T').style.visibility="visible";	
	document.getElementById('T4padlock').style.visibility="hidden"; 
    }
 }
 
function samplesT5(){	
    //setTimeout(call,700);
	setTimeout(muestrasT5,500);
 }
 
function smplesT5_BuMa(){
	 setTimeout(muestrasT5,500);
 }


function muestrasT5(){
	
	let t5sampleqtygood,t5sampleqtybad
	let samplesneural=classifier.getClassExampleCount();

	if (resultadodemuestras==true){//if logic para validar que el modelo a cargar tiene muestras		
	
		//console.log("samplenumbers :"+samplenumbers)
		//console.log("samplesneural[0] :"+samplesneural[0])
		//console.log("samplesnueral[1] :"+samplesneural[1])
		//console.log("sqty :"+sampleqty)

	if (samplesneural[0] != samplesneural[1]){
			document.getElementById('Terminar5T').style.visibility="hidden";
			document.getElementById('T5padlock').style.visibility="visible";
			document.getElementById('T5padlock').innerHTML ="¡ El numero de muestras buenas debe ser igual al numero de muestras malas !"
			samplesneural=classifier.getClassExampleCount();
			document.getElementById('T5samples').innerText = "Pass sample : "+samplenumbers+"\nFail sample : "+samplenumbers;
				}

	 if(samplesneural[0] == samplesneural[1]){
			//console.log("Entra si iguales")
			if(samplesneural[0]== undefined){t5sampleqtygood = samplenumbers; t5sampleqtybad = samplenumbers}
			sampleqty=samplesneural[1];
			document.getElementById('Terminar5T').style.visibility="visible";
			document.getElementById('T5padlock').style.visibility="hidden";
			samplesneural=classifier.getClassExampleCount();
			document.getElementById('T5samples').innerText = "Pass sample : "+t5sampleqtygood+"\nFail sample : "+t5sampleqtybad;
		}
			
	 if (samplesneural[0] > 0){
			//console.log("samplesneural[0] mayor que")
			t5sampleqtygood = samplesneural[0] // + samplenumbers
			document.getElementById('Terminar5T').style.visibility="hidden";
			document.getElementById('T5padlock').style.visibility="hidden";
			document.getElementById('T5samples').innerText = "Pass sample : "+t5sampleqtygood+"\nFail sample : "+t5sampleqtybad;
			
			if(samplesneural[0] == samplesneural[1]){
				sampleqty=samplenumbers + samplesneural[0];
				document.getElementById('Terminar5T').style.visibility="visible";
			}
		}

	 if (samplesneural[1] > 0){
			//console.log("samplesneural[1] mayor que")
			t5sampleqtybad = samplesneural[1] ////+ samplenumbers 
			document.getElementById('Terminar5T').style.visibility="hidden";
			document.getElementById('T5padlock').style.visibility="hidden";
			document.getElementById('T5samples').innerText = "Pass sample : "+t5sampleqtygood+"\nFail sample : "+t5sampleqtybad;
			
			if(samplesneural[0] == samplesneural[1]){
					sampleqty=samplesneural[0];//samplenumbers + 
					document.getElementById('Terminar5T').style.visibility="visible";
				}
		}
			
	}//if logic end
	else {		
		document.getElementById('T5padlock').style.visibility="visible";
		document.getElementById('T5padlock').innerHTML ="¡ Punto no entrenado !"
		samplesneural=classifier.getClassExampleCount();
		//document.getElementById('T5samples').innerText = "Pass sample : "+samplesneural[0]+"\nFail sample : "+samplesneural[1];
		}	
}

//------- Function to call neural network
function load(file, callback) {
  	
     //Can be change to other source	
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
	
    rawFile.send(null);
	
}
																								  
function call(){	
	
	if (resultadodemuestras==true){//if logic
	socket.emit('setuptensorfile',np,actualpoint)	
	}//if logic end
	else {
	 // No hace nada
	}
 }
 
socket.on('loadtensor',function(tensor){//pg migrated
	let data;	
	data=tensor;	
	
    Object.keys(data).forEach((key) => {
    data[key] = tf.tensor2d(data[key],[samplenumbers,1024]) //Shape es [# imagenes,dimension pix] ,[19,1024]    
	});	
	
	console.log(data);
	//Set classifier
	classifier.setClassifierDataset(data);
});

//********************************************************************** Files functions

let picjsonStr,passpicjsonStr,failpicjsonStr,netjsonStr;

function deletenetfile(){   
	net_path="samples/"+np+"_net"+"_point_"+actualpoint+"_neural_network.json"                               
	socket.emit('deletefile',net_path);
}
//------- Function to create a file from pic
async function imgcap(){
	
    let webcam= await tf.data.webcam(webcamElement,xid);
	cap= await webcam.capture();	
	// use Array.from() so when JSON.stringify() it covert to an array string e.g [0.1,-0.2...] 
	// instead of object e.g {0:"0.1", 1:"-0.2"...}	
	picsave=Array.from(cap.dataSync());//	
	picjsonStr = JSON.stringify(picsave);// Original para local storage
}
//------- Function to create a file from good sample
function passpictofile(){
	passpicjsonStr=picjsonStr;
	socket.emit('pictofile', passpicjsonStr);
	console.log("Good sample sent");
	}
//------- Function to create a file from bad sample
function failpictofile(){
	failpicjsonStr=picjsonStr;
	socket.emit('pictofile', failpicjsonStr);
	console.log("Bad sample sent");
	}	

//------- Function to create a file from neuralnetwork	
function jsonnet() {
	
		let dataset = classifier.getClassifierDataset()
		var datasetObj = {}
		let data;
		Object.keys(dataset).forEach((key) => {
	    data = dataset[key].dataSync();
		// use Array.from() so when JSON.stringify() it covert to an array string e.g [0.1,-0.2...] 
		// instead of object e.g {0:"0.1", 1:"-0.2"...}
		datasetObj[key] = Array.from(data); 
		});
		netjsonStr = JSON.stringify(datasetObj);// Original para local storage
		
}
	
function nettofile(){
	
	jsonnet();
	console.log("Neural net to file..");
	setTimeout(socket.emit('nettofile',netjsonStr,np,actualpoint),50);
	setTimeout(setmidpone,60);
	setTimeout(resetclassifier,100);
}


//********************************************************************** Modules

//-----* 42Q 
//let sfdc_flag=false;
function on_42Q(){//Logo de submenus
	document.images["logo_42q"].src = "img/42q_w.jpg"; 
}
function off_42Q(){//Logo de submenus
	document.images["logo_42q"].src = "img/42q_gray.jpg"; 
}
function pass42Q(){
	project = localStorage.getItem('project42Q')
	unit = localStorage.getItem('unit42Q')
	socket.emit('pass',serialnumber,project,unit);
	console.log("42Q Pass sent")
}
function fail42Q(pjct,unitnumber){
	project = localStorage.getItem('project42Q')
	unit = localStorage.getItem('unit42Q')
	socket.emit('fail',serialnumber,project,unit);
	console.log("42Q Fail sent")
}
function setup42Q(project,unit){//Icono de pagina principal
	//document.images["logo_42q"].src = "img/42q_w.jpg"; 
	localStorage.setItem('project42Q', project);
	localStorage.setItem('unit42Q', unit);
	console.log(typeof(project),typeof(unit))
	on_42Q()
}
/*
function setupnotready42Q(){//Icono de pagina principal
	document.images["logo_42q"].src = "img/42q_gray.jpg"; 
}*/
function workstation42Q(serial){
	serialnumber=""+serial+""
	project=localStorage.getItem('project42Q');
	unit=localStorage.getItem('unit42Q');
	socket.emit('workstation',serial,project,unit);
}
function show42Qsetup(){

	project = localStorage.getItem('project42Q')
	unit = localStorage.getItem('unit42Q')
	console.log("Project :"+project+ " Dtype :"+typeof(project))
	console.log("Unit :"+unit +" Dtype :"+typeof(project))
}
function clean42Qsetup(){
	localStorage.removeItem('project42Q');
	localStorage.removeItem('unit42Q');
	off_42Q()
}

//-----* MiddleWare
function mw_on(){//Icono de pagina principal
	document.images["logo_middleware"].src = "img/MiddleWare.jpg";
	
}
function mw_off(){//Icono de pagina principal
	document.images["logo_middleware"].src = "img/MiddleWare_g.jpg";
}

//-----* TCP
function tcp_on(){//Logo de submenus
	document.images["logo_tcp"].src = "img/tcp_on.jpg"; 
}
function tcp_off(){//Logo de submenus
	document.images["logo_tcp"].src = "img/tcp_off.jpg"; 
}

//-----* Point to x*/
let xpointw;
let selectedpoint;
let autotestflag=true;


function opengotopointx(){	
  xpointw= window.open("modules/pointx.html", "xpointw", "width=175,height=170");  
  setTimeout(function movew(){xpointw.moveBy(1100, 200);xpointw.focus();},300);  
}
function gotopointx(xpoint){
	
	if((xpoint <= points)&&(xpoint > 0)){
	pointcounter=xpoint;
	h4T4_status.innerHTML ="Punto(s): "+pointcounter+" de "+points+"";
	//getsampleqty();
	movehw(pointcounter);
	}
	else
	{
	alert("El punto seleccionado no corresponde con algun punto de este modelo =( !!");
	}	
}
function opengotopointxT5(){	
  xpointw= window.open("modules/pointxT5.html", "xpointw", "width=175,height=170");  
  setTimeout(function movew(){xpointw.moveBy(1100, 200);xpointw.focus();},300);  
}
function gotopointxT5(xpoint){
	
	smplesT5_BuMa();
	if((xpoint <= points)&&(xpoint > 0))
	{
		pointcounter=xpoint;
		actualpoint=pointcounter;
		h4T5_status.innerHTML ="Punto(s): "+pointcounter+" de "+points+"";
		getsampleqty();
		document.getElementById('T5samples').innerText = "Pass sample : "+samplenumbers+"\nFail sample : "+samplenumbers;
		movehw(pointcounter);		
			if(samplenumbers!=0){
				setTimeout(call,700);
				}
			
	}
	else{
		alert("El punto seleccionado no corresponde con algun punto de este modelo =( !!");
		}	
}


//-----* Auto test*/
let serialnumber= null  //Variable para utilizar cuando 42Q activado

let multicamtest=true
let tcpiptest=false
let modbustest=false
async function autotest_run(){	
  	  
	//Selecciona el tipo de prueba de acuerdo a configuracion(Multicam,TCP/IP o Modbus)

	if (multicamtest==true){testselection='multicams'}
	if (tcpiptest==true){testselection='tcpip'}
	if (modbustest==true){testselection='modbus'}

    console.log("Test selected :"+testselection)

	switch(testselection) {

	case 'multicams':
		  console.log("Multicams in process...")
		  	//Bandera para activar serial en caso de que 42Q este activado
			if (flag42q==true){serialnumber = prompt("Ingresa el numero de serie:","Serial Number");}
			console.log("SN Captured :"+ serialnumber)
			//**************************************** Fin de bandera 42Q */
		  for (p=1;p<=points;p++)	  
			{
			if (autotestflag==true){// Used in stop option 
				await setcamid();
				console.log("Inicia imageprocess..:"+p);
				await imageprocess(p);//Set to 200ms
				console.log("Inicia predict..:"+p);
				await predict();  //Set to 500ms
				pointcounter=pointcounter+1;
				}
			else{}
			}
			
			console.log("Result_values :"+result_values)
			//result_values=["Punto 1 : Pass","Punto 2 : Fail"]
			
			// Revisa que ningun punto tenga falla para definir flujo
			const failincluded = (elementx) => elementx.includes("Pass")// .includes(true if "Pass" string contained) 
			let multipass = result_values.every(failincluded) //every (Si todos cumple con la condicion true)
			//console.log("passfail :"+multipass)
			camstop()
			if (multipass==true){
				pass42Q()
				result_values=[]
				}
			else{
				fail42Q()
				result_values=[]
			}

			console.log("Multicams executed...")
		  break;
	case 'tcpip':
			console.log("TCP/IP executed")
		  break;
	case 'modbus':
			for (p=1;p<=points;p++)
	  
			{
			if (autotestflag==true){// Used in stop option 
			
			console.log("Inicia movehw punto..:"+p);
			movehw(pointcounter);
			console.log("Inicia position..:"+p);
			await position();
			console.log("Hw en punto..:"+p);
			//getcamid();
			await setcamid();
			console.log("Inicia imageprocess..:"+p);
			await imageprocess(p);//Set to 200ms
			console.log("Inicia predict..:"+p);
			await predict();  //Set to 500ms
			pointcounter=pointcounter+1;
			
			}
			else{}
			} 
			await movehw(pointcounter);
			summarytest();
			setTimeout(gotopointxT5(1),1000);
			camstop();
			autotestflag=true;// Return flag to original state in case it was stoped

			async function position(punto){

				return new Promise(async resolve =>{
				
				console.log("Esperando HW en posicion...");
				document.addEventListener('keyup', (e) => {resolve('resolved');console.log("Pressed");});
				});//Cierra Promise 
				
			}
			//Termina mobus test code
			break;
		default:
		  console.log("No test mode activated")
	  }	  
}
async function imageprocess(punto){
	return new Promise(async resolve =>{ 
	
		smplesT5_BuMa();
	
		pointcounter=punto;
		actualpoint=pointcounter;
		h4T5_status.innerHTML ="Punto(s): "+pointcounter+" de "+points+"";
		h4T4_status.innerHTML ="Punto(s): "+pointcounter+" de "+points+"";
		getsampleqty();
		document.getElementById('T5samples').innerText = "Pass sample : "+samplenumbers+"\nFail sample : "+samplenumbers;
		document.getElementById('T4samples').innerText = "Pass sample : "+samplenumbers+"\nFail sample : "+samplenumbers;
		call();			
		setTimeout(function fire(){resolve('resolved')},200);
		
	});//Cierra Promise
}		
async function setcamid(){		
					 
	return new Promise(async resolve =>{
		
		await getcamid();
		await camstop();
		xid={deviceId:""+webcamid+""};
		await tensorcamactivate();							
		resolve('resolved');
		
	});//Cierra Promise principal
}
function stopautotest(){
	autotestflag=false;
	console.log("Autotest Var: "+autotestflag);
}	
//-----* Summary test*/
let result_values=[];
async function summarytest(){	

	var summary = result_values.toString();
	summary = summary.replace(/,/g," "); 	
	alert ("Resultado Final:\n"+summary);
	result_values=[];// Reinicia valores para presentar de nuevo cuando se llame
	summary = null // Reinicia valores para presentar de nuevo cuando se llame
}

//-----* Multicams*/			
function multicams(){	//cams module webpagecall
  xpointw= window.open("modules/multicams.html", "xpointw", "width=460,height=1000");  
  setTimeout(function movew(){xpointw.moveBy(1000, 400);xpointw.focus();},500);  
}
function mapcams() { //list all cams connected
	
	let meddev=navigator.mediaDevices.enumerateDevices(); 
	navigator.mediaDevices.enumerateDevices().then(gotDevices); 
 
		function gotDevices(devices) { 
		const filtdev = devices.filter(device => device.kind === 'videoinput');	
		console.log(filtdev);
			 
}}
function switchmaincam(selectedcam){ //Switch cam selected by module at TeachMode
	xid=selectedcam;
	const video = document.querySelector('#webcam');	
	navigator.mediaDevices.getUserMedia(selectedcam).
    then((stream) => {video.srcObject = stream});	
	xid={deviceId:""+xid.video.deviceId+""};
	setTimeout(camstop,2000);
	setTimeout(tensorcamactivate,2000);
	webcamid= xid.deviceId; // ID de camara a enviar a DB
	
}
async function defaultcamID(){// Selecciona primer camara del mapping
	
	return new Promise(async resolve =>{
	console.log("Funcion para asignar camara por default")
		let meddev=navigator.mediaDevices.enumerateDevices(); 
		navigator.mediaDevices.enumerateDevices().then(gotDevices); 
 
			function gotDevices(devices) { 
			const filtdev = devices.filter(device => device.kind === 'videoinput');	
			const videoSelect = document.querySelector('select#videoSource');
			xid = {deviceId:""+filtdev[0].deviceId+""};		
			}
			
	setTimeout(function fire(){resolve('resolved')},500);
	});//Cierra Promise	
	console.log("Default camara asignada.")
}
function camstop(){ // Para video
return new Promise(async resolve =>{
	
const video = document.querySelector('video');

// A video's MediaStream object is available through its srcObject attribute
const mediaStream = video.srcObject;

// Through the MediaStream, you can get the MediaStreamTracks with getTracks():
const tracks = mediaStream.getTracks();

// Tracks are returned as an array, so if you know you only have one, you can stop it with: 
//tracks[0].stop();

// Or stop all like so:
tracks.forEach(track => {track.stop();})
resolve('resolved');
	});//Cierra Promise principal

}
async function tensorcamactivate(){
		
		return new Promise(async resolve =>{
		webcam = await tf.data.webcam(webcamElement,xid);//When T4 Active
		webcam5T = await tf.data.webcam(webcamElement5T,xid);// When T5 Active
		resolve('resolved');
			});
}
function vxid(){
	console.log("Xid: ",xid);
}

//-----* Neuralnet trainer*/
function nnettrainer(){	//cams module webpagecall
  xpointw= window.open("modules/nntrainer.html", "xpointw", "width=1000,height=850");  
  setTimeout(function movew(){xpointw.moveBy(1000, 400);xpointw.focus();},500);  
}
//-----* Communication Protocol functions

//-----* Modbus Query
function movehw(hwp){
	
	let modbus_ip = localStorage.getItem('Modbus_IP')
	let modbus_port = localStorage.getItem('Modbus_Port')
	let modbus_xpointreg = localStorage.getItem('Point_reg')
	let modbus_inpositionreg = localStorage.getItem('InPosition_reg')
	
	if(modbus_ip!=undefined&&modbus_port!=undefined&&modbus_xpointreg!=undefined&&modbus_inpositionreg!=undefined){
		
	socket.emit('movehw',hwp,modbus_ip,modbus_port,modbus_xpointreg,modbus_inpositionreg);
	console.log("HW Point Sent: " + hwp)
	}
	else
	{
		console.log("Modbus setup required to move HW");
	}
}
function setupmodbus(modbusip,port,hwpoint,inpositionregister){
	if(modbusip!=undefined&&port!=undefined&&inpositionregister!=undefined){
	localStorage.setItem('Modbus_IP', modbusip);
	localStorage.setItem('Modbus_Port', port);	
	localStorage.setItem('Point_reg',hwpoint);
	localStorage.setItem('InPosition_reg', inpositionregister);
	modbus_on();
	}
	else
	{console.log(" Comando incompleto ");}
}
function cleanmodbus(){
	localStorage.removeItem('Modbus_IP');
	localStorage.removeItem('Modbus_Port');
	localStorage.removeItem('Point_reg');
	localStorage.removeItem('InPosition_reg');
	modbus_off();
}
function readsetupmodbus(){
	let modbus_ip = localStorage.getItem('Modbus_IP')
	let modbus_port = localStorage.getItem('Modbus_Port')
	let modbus_xpointreg = localStorage.getItem('Point_reg')
	let modbus_inpositionreg = localStorage.getItem('InPosition_reg')
	console.log("M_IP: " + modbus_ip);
	console.log("M_Port: " + modbus_port);
	console.log("Point_Reg: " + modbus_xpointreg );
	console.log("Inposition_reg: " + modbus_inpositionreg);
	
}
function modbus_on(){//Icono de pagina principal
	document.images["logo_modbus"].src = "img/modbus.jpg";
	
}
function modbus_off(){//Icono de pagina principal
	document.images["logo_modbus"].src = "img/modbus_gray.jpg";
}

//-----* Remote flags triggers

function remoteflag(){
let arrowup = new Event('keyup');
	arrowup.code = "ArrowUp";
	document.dispatchEvent(arrowup);
}

function testw(){
	let flag=0;
	while(flag < 10){
	console.log("Bandera" + flag);
	flag = flag+1;}
}