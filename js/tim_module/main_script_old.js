
 // Canvas
let canvasjs = document.getElementById('canvas')
let context = canvasjs.getContext('2d')
let fullimage = document.getElementById( 'canvas2' )// Imagen donde se va a mapear
let contextfullimage = fullimage.getContext( '2d' )
let canvica = document.getElementById( 'canvica' )// Imagen donde se va a mapear
let contextcanvica = canvica.getContext( '2d' )
let canvande = document.getElementById( 'canvande' )// Imagen donde se va a mapear
let contextcanvande = canvande.getContext( '2d' )
 
 let image = new Image()
 image.src = '/img/tim_module/LFTM1135558-00-X_1454102-00-A_900px.png'

 let calis = new Image()
 calis.src = '/img/tim_module/camara3.jpg'


 let barprogress=0 // Variable que se ultiliza para incrementar el valor de la barra en la funcion progressbar
 let status="pass" // Variable a utilizar para el estatus por punto
 let logsave=[]  //Variable que utilizamos en la funcion logresult()

function loadimage(){//Funcion Carga la imagen del modelo 
    
    context.drawImage(image, 0, 0, image.width, image.height, 0, 0, context.canvas.width, context.canvas.height);
    
    }

function loadcalis(){//Funcion Carga la imagen del modelo 
    
        contextfullimage.drawImage(calis, 0, 0, calis.width, calis.height, 0, 0, contextfullimage.canvas.width, contextfullimage.canvas.height);
        
 }

//*************************Main test sequence */
   

function bandera(plcinfo){

//... code para handshake
//... code filtrar texto ( serial,np,station)

serialnumber(serial)
modelnumber(np)
station(stationtext)
progressbar(barprogress)
testsequence()

}

async function testsequence() {
    barprogress=0
    progressbar(0)
    loadimage()
    document.getElementById('statusbar').innerHTML = "Inspection in process";
    for (point=1; point<8; point ++){
    
    //whitelight_on()
    await open_cam(point)
    //await captureimage()
    await recorTA(point)
    //inspection()
    //
    pointstatus(point,"pass") // Color del cuadrito debe ser variable
    await stopcam()
    //whitelight_off()
    //blacklight_on()
   // await captureimage(point)
    //inspection()
    //blacklight_off()
    //savepointresult(point)
    //pointstatus(point,status)
    progressbar(barprogress=barprogress+14.5)
     console.log("Punto :"+point)
     //console.log(barprogress)
        if(status == "fail"){alert("Calis de falla detectada !!!"); break; } 
    }
    document.getElementById('statusbar').innerHTML = "Waiting to Start...";
//log_result()
//reset()
}

function serialnumber (serialtext){
    let serial= document.getElementById('sn')
    console.log(serial)
    serial.innerHTML= "Serial: "+serialtext+""
}

function modelnumber (np){
    let model= document.getElementById('np')
    console.log(model)
    model.innerHTML= "Model: "+np+""

}

function station(stationtext){
let station= document.getElementById('st')
console.log(station)
station.innerHTML= "Station: "+stationtext+""



}

function progressbar(percentagevalue){
    
    let bar = document.getElementById('statusbar')
    bar.style.width= ""+percentagevalue+"%"+""   // "20%" --> ""+ 20 +"%"+""

}

function logresult(point,status){

    logsave[point]= status;
logsave[1,2,3,4,5,6,7]
console.log(logsave)
console.log("Esto guardo en la posicion "+point+"Valor"+logsave[4])
}

//********************** Funciones para dibujar recuadros de estatus
function pointstatus(point,status){ 

    if((point == 1)&&(status == 'pass')) {cuadroVerde1();} 
    if((point == 2)&&(status == 'pass')) {cuadroVerde2();}
    if((point == 3)&&(status == 'pass')) {cuadroVerde3();}
    if((point == 6)&&(status == 'pass')) {cuadroVerde4();}
    if((point == 5)&&(status == 'pass')) {cuadroVerde5();}
    if((point == 4)&&(status == 'pass')) {cuadroVerde6();}
    if((point == 7)&&(status == 'pass')) {cuadroVerde7();}
    if((point == 1)&&(status == 'fail')) {cuadroRojo1();}
    if((point == 2)&&(status == 'fail')) {cuadroRojo2();}
    if((point == 3)&&(status == 'fail')) {cuadroRojo3();}
    if((point == 6)&&(status == 'fail')) {cuadroRojo4();}
    if((point == 5)&&(status == 'fail')) {cuadroRojo5();}
    if((point == 4)&&(status == 'fail')) {cuadroRojo6();}
    if((point == 7)&&(status == 'fail')) {cuadroRojo7();}
}
function cuadroVerde1(){ 
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
   
    context.strokeStyle = "#76FF03";
    context.lineWidth = 5;
    context.strokeRect(355, 70, 163, 81);
}
function cuadroVerde2(){ 
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
    
    context.strokeStyle = "#76FF03";
    context.lineWidth = 5;
    context.strokeRect(354, 150, 163, 83);

}
function cuadroVerde3(){ 
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
   
    context.strokeStyle = "#76FF03";
    context.lineWidth = 5;
    context.strokeRect(354, 233, 165, 86);

}
function cuadroVerde4(){ 
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
   
context.strokeStyle = "#76FF03";
context.lineWidth = 5;
context.strokeRect(585, 244, 79, 71);

}
function cuadroVerde5(){ 
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
   
context.strokeStyle = "#76FF03";
context.lineWidth = 5;
context.strokeRect(582, 154, 162, 78);

}
function cuadroVerde6(){ 
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
    
context.strokeStyle = "#76FF03";
context.lineWidth = 5;
context.strokeRect(586, 72, 78, 71);

}
function cuadroVerde7(){ 
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
   
context.strokeStyle = "#76FF03";
context.lineWidth = 5;
context.strokeRect(727, 57, 85, 115);

}
function cuadroRojo1(){  
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
    
    context.strokeStyle = "#FF0000"; // color de figura dentro de un canvas
    context.lineWidth = 5; // tamaño de grosor de la figura
    context.strokeRect(355, 70, 163, 81); // coordenadas de imagen
}
function cuadroRojo2(){  
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
    
    context.strokeStyle = "#FF0000";
    context.lineWidth = 5;
    context.strokeRect(354, 150, 163, 83);
}
function cuadroRojo3(){  
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
    
    context.strokeStyle = "#FF0000";
    context.lineWidth = 5;
    context.strokeRect(354, 233, 165, 86);
}
function cuadroRojo4(){  
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
   
    context.strokeStyle = "#FF0000";
    context.lineWidth = 5;
    context.strokeRect(585, 244, 79, 71);
}
function cuadroRojo5(){ 
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
    
    context.strokeStyle = "#FF0000";
    context.lineWidth = 5;
    context.strokeRect(582, 154, 162, 78);
}
function cuadroRojo6(){  
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
    
    context.strokeStyle = "#FF0000";
    context.lineWidth = 5;
    context.strokeRect(586, 72, 78, 71);
}
function cuadroRojo7(){  
    let canvasjs = document.getElementById('canvas')
    let context = canvasjs.getContext('2d')
   
    context.strokeStyle = "#FF0000";
    context.lineWidth = 5;
    context.strokeRect(727, 57, 85, 115);
}


//*************************************funciones de la camara */
function open_cam(point){// Resolve de 2 segundos

	return new Promise(async resolve =>{
    let camid
    if(point == 1) {camid="6bbc03e9010ca348936ba3691bf214301a5106305eaff95a573c305f9edc21fc"}//1 
    if(point == 2) {camid="436349e3d8d1b6738e04f20418bb4d07e2c7aeaa8ca7ff53c336c1ba76896574"}//2
    if(point == 3) {camid="7104e08d05ca38c42991fade9818169d5a038d70971cec4e5ae452f20c977dc6"}//3
    if(point == 4) {camid="db76624b30de6e85cb6e4e960d9f1785936a6a28ffd3249e7226e810084941e3"}//4 
    if(point == 5) {camid="1781a5b51b5c9942a2d31b378a7836b4d56ee0a3f6a7e3f51a3cbf49e16c46cd"}//5
    if(point == 6) {camid="1511fbb2f4759d77159baac90d59a0958a30c02b3be651ebf99159e974d13840"}//6
    if(point == 7) {camid="d3d01162a63c5869ce3b3337d2233e767387908ad7efe9efe6e4346f3177d697"}//7 
    
    const video = document.querySelector('video');
     const vgaConstraints = {
            video: {deviceId: camid} //width: {exact: 280}, height: {exact: 280} / deviceId: "5bba2c7c9238e1d8ab5e90e2f2f94aa226749826319f6c705c5bfb5a3d2d5279"
        };
            navigator.mediaDevices.getUserMedia(vgaConstraints).
            then((stream) => {video.srcObject = stream});

            setTimeout(function fire(){resolve('resolved');},2000);
        });//Cierra Promise principal
}
function captureimage(){// Resolve de 2 segundos
        return new Promise(async resolve =>{
            
            let image = document.getElementById( 'canvas2' );
            let contexim2 = image.getContext( '2d' );		
                
            var video = document.getElementById("videoElement");
            
            w = image.width;
            h= image.height;
            
            contexim2.drawImage(video,0,0,image.width,image.height);
            //var dataURI = canvas.toDataURL('image/jpeg');
            //savepic(dataURI,point);
            console.log("Sample ready");
        	setTimeout(function fire(){resolve('resolved');},1000);//Temporal para programacion de secuencia
        	});
}
async function recorTA(point){
    return new Promise(async resolve =>{
    switch(point) {
        case 1:
            //TA1
            contextcanvica.drawImage(fullimage,240,332,486,192,0,0,contextcanvica.canvas.width,contextcanvica.canvas.height);
            await analiza()
            //TA2
            contextcanvande.drawImage(fullimage,232,582,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);
            await analiza()
            //TA11
            contextcanvande.drawImage(fullimage,971,787,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);
            await analiza()
            //TA12
            contextcanvande.drawImage(fullimage,1134,389,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);
            await analiza()
          // code block
          break;
        case 2:
          // code block
          break;
        case 3:
            //TA5
            contextcanvica.drawImage(fullimage,1160,751,486,192,0,0,contextcanvica.canvas.width,contextcanvica.canvas.height);
            await analiza()
            //TA6
            contextcanvande.drawImage(fullimage,1139,259,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);
            await analiza()
            //TA7
            contextcanvande.drawImage(fullimage,370,317,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);   
            await analiza()
            //TA8
            contextcanvande.drawImage(fullimage,251,716,538,192,0,0,contextcanvande.canvas.width,contextcanvande.canvas.height);
            await analiza()
            break;
        case 4:
            //TA17
            contextcanvica.drawImage(fullimage,635,603,486,192,0,0,contextcanvica.canvas.width,contextcanvica.canvas.height);
            await analiza()
            // TA18
            contextcanvica.drawImage(fullimage,561,331,486,192,0,0,contextcanvica.canvas.width,contextcanvica.canvas.height);
            await analiza()
            // code block
            break;
        case 5:
            // code block
            break;

        case 6:
            //TA13
            contextcanvica.drawImage(fullimage,978,272,486,192,0,0,contextcanvica.canvas.width,contextcanvica.canvas.height);
            await analiza()
            //TA14
            contextcanvica.drawImage(fullimage,1052,546,486,192,0,0,contextcanvica.canvas.width,contextcanvica.canvas.height);
            await analiza()
            // code block
          break;
        case 7:
            // code block
            break;
        default:
          // code block
      }
      resolve('resolved')})
}
function analiza(){
    return new Promise(async resolve =>{
        setTimeout(function fire(){resolve('resolved');},2000);
    })
}
function mapcams(){
    navigator.mediaDevices.enumerateDevices()
    .then(devices => {
    const filtered = devices.filter(device => device.kind === 'videoinput');			
                         console.log('Cameras found',filtered);
                          });
}
function stopcam(){
    return new Promise(async resolve =>{

        const video = document.querySelector('video');
        // A video's MediaStream object is available through its srcObject attribute
        const mediaStream = video.srcObject;
        // Through the MediaStream, you can get the MediaStreamTracks with getTracks():
        const tracks = mediaStream.getTracks();
        //console.log(tracks);
        // Tracks are returned as an array, so if you know you only have one, you can stop it with: 
        //tracks[0].stop();
        // Or stop all like so:
        tracks.forEach(track => {track.stop()})//;console.log(track);

    setTimeout(function fire(){resolve('resolved');},1000);
    });//Cierra Promise principal
}
//*************************************funciones de fin de secuencia */



